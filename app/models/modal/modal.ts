export interface ModalEl{
    open: (template: string) => void;
    remove: () => void;
     }