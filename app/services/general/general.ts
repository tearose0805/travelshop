
/* Общие методы используются для вставки текста в header   footer*/

/*  -
    - Указать в методах возвращающие типы, типы для параметров, в теле функции также указать типы
*/
import {ITours} from "../../models/tours";
import {getTourTemplate} from "../../templates/tours";
import {openModal} from "@services/modal/modalService";

export function initHeaderTitle(ticketName: string, selector: string): void {
    const headerElement= document.querySelector('header') as HTMLElement;
    const targetItem = headerElement.querySelector(selector) as HTMLElement;
    if (targetItem) {
        targetItem.innerText = ticketName ;
    }
}

export function initFooterTitle(ticketName: string, selector: string): void {
    const headerElement = document.querySelector('footer') as HTMLElement;
    const targetItem = headerElement.querySelector(selector) as HTMLElement;
    if (targetItem) {
        targetItem.innerText = ticketName;
    }
}

export function initToursDivElements(data: ITours[]):void {

    if (Array.isArray(data)) {
        const rootElement = document.querySelector('.main-app') as HTMLDivElement;
        const tourWrap = document.createElement('div') as HTMLDivElement;

        tourWrap.classList.add('tour-wrap');

        // init click for modal
        initTourElemListener(tourWrap);

        let rootElementData: string = '';
        data.forEach((el:ITours, i:number) => {
            rootElementData += getTourTemplate(el, i);
        });

        tourWrap.innerHTML = rootElementData;
        rootElement.appendChild(tourWrap) ;
    }
}

export function initTourElemListener(tourWrap: HTMLDivElement): void {
    tourWrap.addEventListener('click', (ev) => {
        const targetItem = ev.target as HTMLDivElement;
        const parentItem = targetItem?.parentNode as HTMLDivElement;
        let realTarget:HTMLDivElement;

        if (targetItem.hasAttribute('data-tour-item-index')) {
            realTarget = targetItem;
        } else if (parentItem && parentItem.hasAttribute('data-tour-item-index')) {
            realTarget = parentItem;
        }

        if (realTarget) {
            const dataIndex: string = realTarget.getAttribute('data-tour-item-index');
            openModal('order', Number(dataIndex));
        }
    });
}
