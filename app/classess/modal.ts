
// создать в разделе models интерфейс для класса и  применить реализацию к этому классу -ОК
import {ModalEl} from "../models/modal/modal";

export class Modal implements ModalEl {
    private readonly id: string;
    public static modals:Modal[] = [];


    constructor(id: string = null){
        const findModal: Modal = Modal.modals.find((el: Modal)=> {
            return el.id === id;
        });

        if(findModal){
            Modal.removeById(id);
        }
        Modal.modals.push(this);
        this.id = id || (Math.random() + Modal.modals.length).toString();
    }

    public open(template: string):void {
        const divWrap: HTMLDivElement = document.createElement("div");
        divWrap.innerHTML = template;
        divWrap.id = this.id;
        divWrap.setAttribute('modal_id', this.id);
        divWrap.classList.add('modal-element');
        divWrap.addEventListener('click', this.closeModalHandler);
        document.body.appendChild(divWrap);
}

    public remove():void{
        const el = document.getElementById(this.id) as HTMLElement;
        if(el){
            el.removeEventListener('click',this.closeModalHandler);
            el.parentNode.removeChild(el);
        }
    }

    public static removeById(id: string):void{
        let modalId: string = id;
        const findEl: Modal = Modal.modals.find((el) => {
            return el.id === modalId
        });
        if(findEl){
            findEl.remove();
            Modal.modals = Modal.modals.filter((el: Modal) =>{
                return el.id!==modalId
            });
          }
      }

    private closeModalHandler = (ev: Event): void => {
        const target = ev.target as HTMLElement;
        if (target.classList.contains('close-modal')) {
            this.remove();
        }
    }

    public static removeAll(): void{
        if(Array.isArray(Modal.modals)){
            Modal.modals.forEach((el: Modal) =>Modal.removeById(el.id))
        }
    }
}